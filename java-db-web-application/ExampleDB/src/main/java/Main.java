import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Main {

    public static void main(String[] args) throws SQLException {
        Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/JDBC_Work",
                "postgres",
                "micron20014");


        ResultSet resultSet = connection.createStatement().executeQuery("SELECT * FROM users;");
        resultSet.next();
        System.out.println(resultSet.getString(1) + " " + resultSet.getString("username"));
    }
}

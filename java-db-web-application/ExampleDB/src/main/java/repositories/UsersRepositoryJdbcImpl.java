package repositories;

import models.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


public class UsersRepositoryJdbcImpl implements UsersRepository<Long> {

    //language=SQL
    private static final String SQL_FIND_ALL = "SELECT * FROM users;";
    //language=SQL
    private static final String SQL_FIND_BY_ID = "SELECT * FROM users WHERE id = ?;";
    //language=SQL
    private static final String SQL_INSERT_USER = "INSERT INTO users(uuid, username, hashpassword) VALUES (?, ?, ?);";
    //language=SQL
    private static final String SQL_DELETE_USER = "DELETE FROM users WHERE id = ?;";


    private final Connection connection;

    public UsersRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void save(User user) {
        try {
            PreparedStatement ps = connection.prepareStatement(SQL_INSERT_USER);
            ps.setString(1, user.getUuid());
            ps.setString(2, user.getUsername());
            ps.setString(3, user.getHashPassword());
            ps.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Long id) {
        try {
            PreparedStatement ps = connection.prepareStatement(SQL_DELETE_USER);
            ps.setLong(1, id);
            ps.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User user) {
        this.delete(user.getId());
        this.save(user);
    }


    @Override
    public Optional<User> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public List<User> findAll() {
        return null;
    }


    @Override
    public List<User> findByUUID(String uuid) {
        return null;
    }

    @Override
    public List<User> findByUsername(String username) {
        return null;
    }
}
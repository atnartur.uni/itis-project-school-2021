package repositories;

import models.User;

import java.util.List;

public interface UsersRepository<ID> extends CrudRepository<User, ID> {

    List<User> findByUUID(String uuid);
    List<User> findByUsername(String username);

}

package ru.itis.rasimusv.repositories;

import ru.itis.rasimusv.models.User;

public interface UsersRepository extends CrudRepository<User> { }
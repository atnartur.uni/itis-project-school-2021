package ru.itis.rasimusv.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.itis.rasimusv.models.User;
import ru.itis.rasimusv.services.UsersService;

@Controller
public class UsersController {

    @Autowired
    private UsersService usersService;


    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String getUsersPage(Model model) {
        model.addAttribute("users", usersService.getAllUsers());
        return "users_view";
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public String addUser(@RequestParam(value = "firstName", required = true) String firstName,
                          @RequestParam(value = "lastName", required = true) String lastName) {
        usersService.addUser(User
                .builder()
                .firstName(firstName)
                .lastName(lastName)
                .build());
        return "redirect:/users";
    }
}

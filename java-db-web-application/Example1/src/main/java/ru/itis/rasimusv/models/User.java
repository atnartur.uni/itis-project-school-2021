package ru.itis.rasimusv.models;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.EqualsAndHashCode;
import lombok.ToString;


@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class User {
    private String firstName;
    private String lastName;
}

package ru.itis.rasimusv.services;

import ru.itis.rasimusv.models.User;

import java.util.List;


public interface UsersService {
    List<User> getAllUsers();
    void addUser(User user);
}

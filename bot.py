from pprint import pprint

import telebot
from telebot.types import Message

# pip install pyTelegramBotAPI

TOKEN = 'TOKEN'

bot = telebot.TeleBot(TOKEN, parse_mode=None) # You can set parse_mode by default. HTML or MARKDOWN



@bot.message_handler(commands=['start', 'help'])
def send_welcome(message: Message):
	print(message)
	bot.reply_to(message, "Привет, мир!")


@bot.message_handler(commands=['info'])
def send_info(message: Message):
	print(message)
	bot.reply_to(message, f"Тебя зовут {message.from_user.first_name}")


# @bot.message_handler(func=lambda message: 'помоги' in getattr(message, 'text', ''))
# def help(message):
# 	bot.reply_to(message, f'Ты сказал "{message.text}, чем тебе помочь?')


@bot.message_handler(content_types=["photo"])
def echo_all(message):
	chat_id = message.chat.id
	print(chat_id)
	bot.reply_to(message, 'принято')


print('started')
bot.polling()
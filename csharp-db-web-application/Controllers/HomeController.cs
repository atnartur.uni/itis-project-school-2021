﻿using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        [Route("")]
        public IActionResult Main()
        {
            var model = new MainViewModel
            {
                People = DAO.DAO.GetPeople()
            };
            return View(model);
        }

        [HttpPost]
        [Route("")]
        public IActionResult Main(string firstName, string lastName, int age)
        {
            DAO.DAO.AddPeople(firstName, lastName, age);
            var model = new MainViewModel
            {
                People = DAO.DAO.GetPeople()
            };
            return View(model);
        }
    }
}

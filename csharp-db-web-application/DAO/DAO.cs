﻿using Npgsql;
using System.Collections.Generic;
using WebApplication.Models;

namespace WebApplication.DAO
{
    public class DAO
    {
        private static readonly string connectionString = string.Format("Server={0};Port={1};User Id={2};Password={3};Database={4};",
               "localhost", "5432", "postgres", "postgres", "test");
        public static List<PersonViewModel> GetPeople()
        {
            var people = new List<PersonViewModel>();
            var sqlExpression = "SELECT firstname, lastname, age FROM person";
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                NpgsqlCommand command = new NpgsqlCommand(sqlExpression, connection);
                NpgsqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var person = new PersonViewModel
                        {
                            FirstName = reader.GetValue(0).ToString(),
                            LastName = reader.GetValue(1).ToString(),
                            Age = (int)reader.GetValue(2)
                        };
                        people.Add(person);
                    }
                }
                connection.Close();
            }
            return people;
        }

        public static void AddPeople(string firstname, string lastname, int age)
        {
            var sqlExpression = string.Format("INSERT INTO person (firstname, lastname, age) VALUES ('{0}', '{1}', {2})",
                firstname, lastname, age);
            using NpgsqlConnection connection = new NpgsqlConnection(connectionString);
            connection.Open();
            NpgsqlCommand command = new NpgsqlCommand(sqlExpression, connection);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
}

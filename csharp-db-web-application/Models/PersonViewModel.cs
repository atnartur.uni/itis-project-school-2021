﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public class PersonViewModel
    {
        public int Id;
        public string FirstName;
        public string LastName;
        public int Age;
    }
}

# Работа с SQLite с помощью SQLAlchemy

Запуск:
1. `pip install -r requirements.txt` - устанавливаем зависимости из файла requirements.txt
2. `python main.py` - запускаем скрипт для того, чтобы появился файл с базой данных (`db.sqlite3`)
3. Подключаемся к базе данных: [с помощью PyCharm](https://www.jetbrains.com/help/pycharm/connecting-to-a-database.html#connect-to-sqlite-database) или любого другого инструмента для работы с базой данных
4. Выполняем запросы для создания таблицы. Они указаны в комментариях к моделям в `main.py`
5. Запускаем `main.py`, который будет выполнять запросы к базе данных 

Больше информации на https://docs.sqlalchemy.org/en/13/orm/tutorial.html

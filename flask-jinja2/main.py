from flask import Flask, render_template, request

app = Flask(__name__)


@app.route('/')
def hello_world():
    """Обработчик, выводящий строчку на странице сайта"""
    return 'Hello, World!'


@app.route('/page', methods=['GET', 'POST'])
def templated_page():
    """Обработчик для вывода шаблона и обоработки POST-запроса"""
    extra_context = {}
    if request.method == 'POST':
        # если пришел POST-запрос, отправляем все данные из формы на вывод
        extra_context = request.form

    return render_template(
        'main.html',  # название шаблона для отображения. Все шаблоны должны быть в папке templates
        value_from_flask='Flask hello',  # переменная для отображения в шаблоне
        **extra_context  # словарь с дополнительными переменными для вывода на сайте
    )


if __name__ == '__main__':
    # запуск фреймворка
    app.run()

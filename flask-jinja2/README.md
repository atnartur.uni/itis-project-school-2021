# Работа с Flask и Jinja2

Запуск:
1. `pip install -r requirements.txt` - устанавливаем зависимости из файла requirements.txt
2. `python main.py` - запускаем сервер Flask

Больше информации:

- [Flask - быстрый старт](https://flask.palletsprojects.com/en/1.1.x/quickstart/)
- [Работа с шаблонами в Flask](ttps://flask.palletsprojects.com/en/1.1.x/tutorial/templates/)
